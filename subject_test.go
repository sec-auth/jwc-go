package main

import (
	"testing"

	"encoding/json"
)

func TestJsonDeserialization(t *testing.T) {
	jsonBytes := []byte("{ \"commonName\": \"dsdf-gfdf-asda-werss-23123\" }")

	sub := &Subject{}

	if err := json.Unmarshal(jsonBytes, sub); err != nil {
		t.Fatal("Unmarshal of Subject failed", sub)
	}

	cn, err1 := sub.CommonName()
	if err1 != nil {
		t.Fatal("common name not parsed", err1)
	}

	if cn != "dsdf-gfdf-asda-werss-23123" {
		t.Fatal("common name not parsed correctly")
	}

}
