package main

import (
	"errors"
	"fmt"
)

const VERIFY_MAX_STEPS = 5

var (
	ERROR_CHAIN_TOO_LONG               = errors.New(fmt.Sprintf("more than %d certificates in the chain", VERIFY_MAX_STEPS))
	ERROR_ROOT_CERTIFICATE_NOT_TRUSTED = errors.New("root certificate not trusted")
	ERROR_UNKNOWN_PARENT               = errors.New("COULD NOT VERIFY CERTIFICATE - PARENT UNKNOWN")
	ERROR_CERTIFICATE_EXPIRED          = errors.New("CERTIFICATE EXPIRED")
	ERROR_WRONG_USAGE                  = errors.New("THE CERTIFICATE HAS NOT THE REQUIRED USAGE")
)

func (jwc *JSONWebCertificate) Verify(roots CaRootPool) error {
	return jwc.verifyRecursive(roots, 0)
}

func (jwc *JSONWebCertificate) verifyRecursive(roots CaRootPool, steps int) error {
	if steps > VERIFY_MAX_STEPS {
		return ERROR_CHAIN_TOO_LONG
	} else if jwc.IsSelfSigned() {
		if err := jwc.VerifyWith(jwc); err != nil {
			return err
		}

		if roots.IsTrustedRoot(jwc) {
			return nil
		} else {
			return ERROR_ROOT_CERTIFICATE_NOT_TRUSTED
		}
	} else if jwc.Parent != nil {
		if err := jwc.VerifyWith(jwc.Parent); err != nil {
			return err
		}

		return jwc.Parent.verifyRecursive(roots, steps+1)
	} else {
		return ERROR_UNKNOWN_PARENT
	}
}

func (jwc *JSONWebCertificate) VerifyWith(parent *JSONWebCertificate) error {
	parentContent, err1 := parent.GetContent()
	if err1 != nil {
		return err1
	}

	content, err2 := jwc.GetContent()
	if err2 != nil {
		return err2
	}

	if !content.IsValidNow() {
		return ERROR_CERTIFICATE_EXPIRED
	}

	if !parentContent.HasUsage(VERIFY_CERTIFICATE) {
		return ERROR_WRONG_USAGE
	}

	if _, err := jwc.Signature.Verify(parentContent.Key); err != nil {
		return err
	}

	return nil
}
