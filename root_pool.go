
package main

type CaRootPool interface {
	IsTrustedRoot(jwc *JSONWebCertificate) bool
}
