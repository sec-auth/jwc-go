package main

import "testing"

func TestSerialNumber(t *testing.T) {
	s1 := GenerateSerialNumber()

	if len(s1) < 64 {
		t.Fatal("Len of serial number is too short")
	}

	tests := make([]string, 10000)

	for i := 0; i < len(tests); i++ {
		tests[i] = GenerateSerialNumber()
	}

	for i, current := range tests {
		for j := 0; j < i; j++ {
			if current == tests[j] {
				t.Fatal("Serial number is not unique")
			}
		}
	}
}

func TestGenerateId(t *testing.T) {
	s1 := GenerateId()

	if len(s1) < 16 {
		t.Fatal("Len of serial number is too short")
	}

	tests := make([]string, 10000)

	for i := 0; i < len(tests); i++ {
		tests[i] = GenerateId()
	}

	for i, current := range tests {
		for j := 0; j < i; j++ {
			if current == tests[j] {
				t.Fatal("Serial number is not unique")
			}
		}
	}
}
