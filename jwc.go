package main

import (
	jose "gopkg.in/square/go-jose.v2"

	"encoding/json"

	"hash"

	"crypto/sha256"
)

type JSONWebCertificate struct {
	Signature *jose.JSONWebSignature
	Parent    *JSONWebCertificate

	content *ProtectedContent
}

// Content

func (jwc *JSONWebCertificate) GetContent() (*ProtectedContent, error) {
	if jwc.content != nil {
		return jwc.content, nil
	}
	c, err := jwc.parseContent()
	if err != nil {
		jwc.content = c
		return c, nil
	} else {
		return nil, err
	}
}

func (jwc *JSONWebCertificate) parseContent() (*ProtectedContent, error) {
	bytes := jwc.Signature.UnsafePayloadWithoutVerification()
	c := &ProtectedContent{}
	if err := json.Unmarshal(bytes, c); err != nil {
		return nil, err
	} else {
		return c, nil
	}
}

func (jwc *JSONWebCertificate) IsSelfSigned() bool {
	signatureKid := jwc.Signature.Signatures[len(jwc.Signature.Signatures)-1].Header.KeyID
	keyKid := jwc.content.Key.KeyID
	return signatureKid == keyKid
}

func (jwc *JSONWebCertificate) Fingerprint(h hash.Hash) []byte {
	h.Write(jwc.Signature.UnsafePayloadWithoutVerification()) // Writes to Hash never retur an error
	return h.Sum(nil)
}

func (jwc *JSONWebCertificate) FingerprintSha256() []byte {
	return jwc.Fingerprint(sha256.New())
}
