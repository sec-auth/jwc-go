package main

import (
	"encoding/json"
	"errors"

	jose "gopkg.in/square/go-jose.v2"
)

type jwcJsonDto struct {
	Parent    *JSONWebCertificate `json:"parent"`
	Signature json.RawMessage     `json:"signature"`
}

// JSON Serialization

var _ json.Marshaler = &JSONWebCertificate{}
var _ json.Unmarshaler = &JSONWebCertificate{}

func (jwc *JSONWebCertificate) MarshalJSON() ([]byte, error) {
	sig := jwc.Signature.FullSerialize()
	dto := &jwcJsonDto{
		Parent:    jwc.Parent,
		Signature: []byte(sig),
	}

	return json.Marshal(dto)
}

func (jwc *JSONWebCertificate) UnmarshalJSON(data []byte) error {
	dto := &jwcJsonDto{}
	if err := json.Unmarshal(data, dto); err != nil {
		return err
	}

	sig, err := jose.ParseSigned(string(dto.Signature))
	if err != nil {
		return err
	}

	jwc.Signature = sig
	jwc.Parent = dto.Parent

	if jwc.Signature == nil {
		return errors.New("the signature property must be present")
	}

	return nil
}
