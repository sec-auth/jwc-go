package main

import (
	"fmt"
)

func panicNil(v any, name string) {
	if v == nil {
		panic(fmt.Sprintf("Property %s must not be nil", name))
	}
}
