package main

import (
	"errors"

	jose "gopkg.in/square/go-jose.v2"

	"time"

	"encoding/json"

	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
)

func CreateSigner(priv any) (jose.Signer, error) {
	var alg jose.SignatureAlgorithm

	switch priv.(type) {
	case rsa.PrivateKey:
		alg = jose.RS512
	case ecdsa.PrivateKey:
		alg = jose.ES512
	default:
		return nil, errors.New("type of priv is not a valid private key")
	}

	sig, err := jose.NewSigner(jose.SigningKey{
		Algorithm: alg,
		Key:       priv,
	}, nil)

	return sig, err
}

type NewCertificateOptions struct {
	Parent     *JSONWebCertificate
	ParentPriv any
	PublicKey  *jose.JSONWebKey
	Subject    Subject
	Usage      []Usage
	Authority  string
}

func NewCertificate(op NewCertificateOptions) (*JSONWebCertificate, error) {

	content := &ProtectedContent{
		SerialNumber: GenerateSerialNumber(),
		Authority:    op.Authority,
		NotBefore:    time.Now(),
		NotAfter:     time.Now().AddDate(4, 0, 0),
		Key:          op.PublicKey,
		Subject:      op.Subject,
		Usage:        op.Usage,
	}

	contentBytes, err := json.Marshal(content)
	if err != nil {
		return nil, err
	}

	signer, err := CreateSigner(op.ParentPriv)
	if err != nil {
		return nil, err
	}

	signature, err := signer.Sign(contentBytes)
	if err != nil {
		return nil, err
	}

	cert := &JSONWebCertificate{
		Signature: signature,
		Parent:    op.Parent,
		content:   content,
	}

	return cert, nil
}

type SelfSignedCertificateOptions struct {
	PublicKey  *jose.JSONWebKey
	PrivateKey any
	Subject    Subject
	Usage      []Usage
	Authority  string
}

func NewSelfSignedCertificate(op SelfSignedCertificateOptions) (*JSONWebCertificate, error) {
	content := &ProtectedContent{
		SerialNumber: GenerateSerialNumber(),
		Authority:    op.Authority,
		NotBefore:    time.Now(),
		NotAfter:     time.Now().AddDate(4, 0, 0),
		Key:          op.PublicKey,
		Subject:      op.Subject,
		Usage:        op.Usage,
	}

	contentBytes, err := json.Marshal(content)
	if err != nil {
		return nil, err
	}

	signer, err := CreateSigner(op.PrivateKey)
	if err != nil {
		return nil, err
	}

	signature, err := signer.Sign(contentBytes)
	if err != nil {
		return nil, err
	}

	cert := &JSONWebCertificate{
		Signature: signature,
		Parent:    nil,
		content:   content,
	}

	return cert, nil
}

func NewCaRootCertificate() (*JSONWebCertificate, any, error) {
	priv, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		return nil, nil, err
	}

	pub := &jose.JSONWebKey{
		Key:   priv.Public(),
		KeyID: GenerateId(),
	}

	op := SelfSignedCertificateOptions{
		PublicKey:  pub,
		PrivateKey: priv,
		Subject: Subject{
			"commonName": GenerateId(),
		},
		Usage: []Usage{VERIFY_CERTIFICATE, VERIFY, ENCRYPT},
	}

	cert, err := NewSelfSignedCertificate(op)
	if err != nil {
		return nil, nil, err
	}

	return cert, priv, nil

}
