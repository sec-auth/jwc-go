package main

import (
	"encoding/base64"
)

type SimpleRootPool map[string]struct{}

var _ CaRootPool = &SimpleRootPool{}

func NewSimpleRootPool() SimpleRootPool {
	return SimpleRootPool{}
}

func (p SimpleRootPool) hashJwc(jwc *JSONWebCertificate) string {
	return base64.URLEncoding.EncodeToString(jwc.FingerprintSha256())
}

func (p SimpleRootPool) AddCertificate(jwc *JSONWebCertificate) {
	h := p.hashJwc(jwc)
	p[h] = struct{}{}
}

func (p SimpleRootPool) IsTrustedRoot(jwc *JSONWebCertificate) bool {
	h := p.hashJwc(jwc)
	_, ok := p[h]
	return ok
}
