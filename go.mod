module gitlab.com/sec-auth/jwc

go 1.19

require (
	golang.org/x/crypto v0.5.0 // indirect
	gopkg.in/square/go-jose.v2 v2.6.0 // indirect
)
