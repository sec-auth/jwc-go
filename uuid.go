package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
)

func GenerateSerialNumber() string {
	const LEN = 64
	bytes := make([]byte, LEN)

	if len, err := rand.Reader.Read(bytes); err != nil || len != LEN {
		panic(fmt.Sprintln("Could not generate random bytes of length ", LEN))
	}

	return base64.URLEncoding.EncodeToString(bytes)
}

func GenerateId() string {
	const LEN = 16
	bytes := make([]byte, LEN)

	if len, err := rand.Reader.Read(bytes); err != nil || len != LEN {
		panic(fmt.Sprintln("Could not generate random bytes of length ", LEN))
	}

	return base64.URLEncoding.EncodeToString(bytes)
}
