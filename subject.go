package main

import "errors"

type Subject map[string]string

func (s Subject) CommonName() (string, error) {
	cn, exists := s["commonName"]
	if exists {
		return cn, nil
	} else {
		return "", errors.New("commonName is not present")
	}
}
