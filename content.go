package main

import (
	jose "gopkg.in/square/go-jose.v2"

	"time"
)

type ProtectedContent struct {
	SerialNumber string           `json:"serialNumber"`
	Authority    string           `json:"authority"`
	NotBefore    time.Time        `json:"notBefore"`
	NotAfter     time.Time        `json:"NotAfter"`
	Key          *jose.JSONWebKey `json:"key"`
	Subject      Subject          `json:"subject"`
	Usage        []Usage          `json:"usage"`
}

func (c *ProtectedContent) IsValid(t time.Time) bool {
	panicNil(t, "t")
	return c.NotAfter.After(t) &&
		c.NotBefore.Before(t) &&
		c.Key.IsPublic()
}

func (c *ProtectedContent) IsValidNow() bool {
	return c.IsValid(time.Now())
}

func (c *ProtectedContent) HasUsage(usage Usage) bool {
	for _, el := range c.Usage {
		if el == usage {
			return true
		}
	}

	return false
}
