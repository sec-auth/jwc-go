package main

type Usage string

const (
	VERIFY             Usage = "VERIFY"
	ENCRYPT            Usage = "ENCRYPT"
	VERIFY_CERTIFICATE Usage = "VERIFY_CERTIFICATE"
)
